import 'package:pipelinenet/pipelinenet.dart';
import 'package:pipelinenet/src/manifold.dart';
import 'package:pipelinenet/src/pipeline.dart';
import 'package:test/test.dart';

Future<void> main() async {
  Manifold mf = Manifold();
  group('A group of tests', () {

    
    mf["Int_to_String"] = PipeLine<int,String>(handler: (int val){
      print('Int_to_String: $val');
      return val.toString();
    });
    mf["String_to_Int"] = PipeLine<String,int>(handler: (String val){
      print('String_to_Int: $val');
      return int.tryParse(val);
    });
    mf["Int_to_String"]?.connect(mf["String_to_Int"]!);
    mf["String_to_Int"]?.output.listen((value) {
      print('Result: $value');
    });
    
    test('First Test', () async{
      // print(${mf.pipelines.keys});
      expect(mf.send('Int_to_String', 10), isTrue);
      await Future.delayed(Duration(seconds: 1));
      await mf["Int_to_String"]!.disconnect(mf["String_to_Int"]!);
      expect(mf.send('Int_to_String', 11), isTrue);
      await Future.delayed(Duration(seconds: 1));

    });
  });
}
